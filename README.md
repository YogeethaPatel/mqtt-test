To start Node Server
step 1: Navigate to Server folder
step 2: run 'npm install' in terminal
step 3: run 'npm start' 
step 4: Browse 'https://localhost:3000/api-doc' to launch the Node server

To start Angular 
step 1: Navigate to Client folder
step 2: run 'npm install' in terminal
step 3: run 'npm start' 
step 4: Browse 'https://localhost:4200' to launch Angular App



