const express = require('express');
const app = express();
const mqtt = require('./utility/mqtt.js');
const swaggerUI = require("swagger-ui-express");
const swaggerConfigJson = require('./swagger.json'); 
const cors = require('cors');
const { json } = require('express');
var subscribedTopic =  "ltts-mqtt-demo";
var interval = null;
app.use(cors());
app.use(express.json());

/**
 * Define API docs path and Load the SwaggerConfiguration
 */
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerConfigJson));

/**
 * MQTT Client Subscriber and Publisher Code
 */

/**
 * MQTT Test broker Instance, Also can be configured our own MQTT broker.
 */
 const host = 'mqtt://test.mosquitto.org'; 
 /**
  * MQTT Client Instance
  */
 var client  = mqtt.connectInstance(host);
 /**
  * Subscribe and listen to get the status of connection
  */
 client.on('connect',()=>{
     console.log(`Connection Status:  ${client.connected}`);
     subscribeTopic();
 });
 /**
  * Subscribe and listen to get error instance, if it fail to connect.
  */
 client.on('error',(error)=>{
    console.log(`Error Occured while connecting:  ${error}`);
 });
 /**
  * Subscribe and listen to receive client published messages
  */
 client.on('message',(topic, message)=>{
     console.log(`Message Received from the Client: ${message}`);
 });

 function subscribeTopic(){
     client.subscribe(subscribedTopic);
 }

 function pushRandomMessageToClient(){
     if(client.connected){
         let message = {
             timeStamp: Date.now().toLocaleString(),
             value: Math.floor(Math.random() * 1000)
         }
         client.publish(subscribedTopic, JSON.stringify(message));
     }
 }

 function startOrStopInterval(mode){
    if(mode.toLowerCase() === "start"){
        clearInterval(interval);
        interval = setInterval(pushRandomMessageToClient,100);
    } else{
        clearInterval(interval);
        interval = null;
    }
 }

/**Ends */
/**
 * API Endpoint used to "Start" or "Stop" publishing message to the client
 */
app.post('/api/postOperationStatus',(req,res)=>{
    let mode = req.body.mode;
    if(mode.toLowerCase() !== "start" && mode.toLowerCase() !=="stop"){
        res.status(400).json({"error_message": "Invalid Operation Mode"});
        return;
    }
    let result = "";
    if(mode.toLowerCase()==="stop" && interval === null){
        result = { status: "Already Stopped", isPublished: false}
    } else{
        startOrStopInterval(mode);
        result = {"status": mode.toLowerCase()==="start" ? "Started to publish the message to the cilent" : "Stopped", isPublished: interval !== null ? true: false};
    }
    res.status(200).send(JSON.stringify(result));    
})
/**
 * API endpoint used to get the client status
 */
app.get('/api/getClientStatus',(req, res)=>{
    let result = interval === null ? {status: "STOPPED", topic: subscribedTopic} : {status: "RUNNING", topic: subscribedTopic};
    res.status(200).send(JSON.stringify(result));
})

module.exports = app;