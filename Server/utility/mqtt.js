/**
 * MQTT wrapper file
 */
var mqtt = require('mqtt');

var mqttWrapper = function(){
    return (
        {
            connectInstance: ConnectInstance
        }
    )
}

/**
 * This method is used connect the specified broker URL and return a client Instance
 * @param {MQTT broker URL} host 
 * @returns  Client Instance
 */
function ConnectInstance(host){
  return mqtt.connect(host);
}

module.exports = mqttWrapper();