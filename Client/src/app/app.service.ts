import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  apiHost: string = "http://localhost:3000/api/"
  constructor(public http: HttpClient) { }

  getClientStatus():Observable<any>{
    return this.http.get(this.apiHost + "getClientStatus");
  }

  postOperationStatus(mode: string): Observable<any> {
    return this.http.post(this.apiHost + "postOperationStatus", {mode});
  }
}
