import { stringify } from '@angular/compiler/src/util';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'HDSUI client';
  mqttStatus: any[] = [];
  mode: string= "";
  operationStatus: any[] = [];
  constructor(public appService: AppService){}
  
  getClientStatus(){
     this.appService.getClientStatus().subscribe((result)=>{
      this.mqttStatus.push(result);
    });
  }

  postOperation(){
    if(this.mode){
      this.appService.postOperationStatus(this.mode).subscribe((result)=>{
        if(result.isPublished){
          this.mqttStatus = [];
          this.operationStatus = [];
        }
        this.operationStatus.push(result);
      });
    }   
  }
}
